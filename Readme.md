# Coding Challenge Solution
# Requirements
- Ruby = 2.3

# Setup
- Goto project's folder in your terminal
- run `bundle install`
- now run `ruby bin/bakery_executor.rb`
- enter reservation requests on stdin or pass a filename

# Specs
if you want to run the specs please run `bundle exec rspec spec` in project's home directory.
